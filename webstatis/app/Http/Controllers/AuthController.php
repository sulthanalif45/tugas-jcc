<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page.register');
    }

    public function welcome(Request $request){
        $nama = $request['firs_name']." ".$request['last_name'];
        return view('page.welcome', compact('nama'));
    }
}
