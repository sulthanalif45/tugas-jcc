<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@dashboard');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/table', 'AdminController@table');
Route::get('/data-table', 'AdminController@dataTable');

//menampilkan data / index
Route::get('/cast', 'CastController@index');
//form input data
Route::get('/cast/create', 'CastController@create');
//input data
Route::post('/cast', 'CastController@store');
//detail
Route::get('/cast/{cast_id}', 'CastController@show');
//form edit
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//update data
Route::put('/cast/{cast_id}', 'CastController@update');
//hapus data
Route::delete('/cast/{cast_id}', 'CastController@destroy');