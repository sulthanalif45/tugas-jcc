@extends('layout.main')
@section('title')
    Buat Account Baru  
@endsection
@section('content')
<form method="post" action="/welcome">
    @csrf
    <label for="firs_name">Firs name : </label><br><br>
    <input type="text" id="firs_name" name="firs_name"><br><br>
    
    <label for="last_name">Last name : </label><br><br>
    <input type="text" id="last_name" name="last_name"><br><br>

    <label for="gender">Gender</label><br><br>
    <input type="radio" name="gender">Male <br>
    <input type="radio" name="gender">Female <br><br>

    <label for="nationality">Nationality</label><br><br>
    <select name="nationality" id="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Amerika">Amerika</option>
        <option value="Inggris">Inggris</option>
    </select><br><br>

    <label for="language">Language Spoken</label><br><br>
    <input type="checkbox" name="language" id="language">Bahasa Indoensia <br>
    <input type="checkbox" name="language" id="language">English <br>
    <input type="checkbox" name="language" id="language">Other <br><br>

    <label for="bio">Bio</label><br><br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

    <input type="submit" name="submit" value="Sign Up">
</form>
@endsection    