@extends('layout.main')
@section('title')
    Detail
@endsection
@section('content')
    <table border="0" style="font-size: 30px">
        <tr>
            <td>
                Nama
            </td>
            <td>
                :
            </td>
            <td>
                {{ $cast->nama }}
            </td>
        </tr>
        <tr>
            <td>
                Umur
            </td>
            <td>
                :
            </td>
            <td>
                {{ $cast->umur }} Thn
            </td>
        </tr>
        <tr>
            <td>
                Bio
            </td>
            <td>
                :
            </td>
            <td>
                {{ $cast->bio }}
            </td>
        </tr>
    </table>
    <a href="/cast" class="btn btn-primary mt-4">Kembali</a>
@endsection