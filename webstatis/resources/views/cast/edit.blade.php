@extends('layout.main')
@section('tittle')
    Edit
@endsection
@section('content')
<form method="post" action="/cast/{{ $cast->id }}">
    @csrf
    @method('put')
    <label for="nama">Nama : </label><br>
    <input type="text" id="nama" name="nama" value="{{ $cast->nama }}" ><br><br>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label for="umur">Umur :</label><br>
    <input type="number" id="umur" name="umur" value="{{ $cast->umur }}"><br><br>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label for="bio">Bio</label><br>
    <textarea name="bio" id="bio" cols="30" rows="10">{{ $cast->bio }}</textarea><br>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <a href="/cast" class="btn btn-danger">Kembali</a>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection