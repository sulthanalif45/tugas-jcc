@extends('layout.main')
@section('title')
    Cast
@endsection
@section('content')
<a href="/cast/create"class="btn btn-primary mb-3">Tambah Data</a>
<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>#</th>
      <th>Nama</th>
      <th>Umur</th>
      <th>Bio</th>
      <th>Aksi</th>
    </tr>
    </thead>
    <tbody>
    @forelse ($cast as $key => $data)
        <tr>
        <td>{{ $key + 1 }}</td>
        <td>{{ $data->nama }}</td>
        <td>{{ $data->umur }}</td>
        <td>{{ $data->bio }}</td>
        <td width="20%">
            <form action="/cast/{{ $data->id }}" method="POST">
                @csrf
                @method('delete')
                <a href="/cast/{{ $data->id }}" class="btn btn-primary btn-sm">Detail</a>|
                <a href="/cast/{{ $data->id }}/edit" class="btn btn-success btn-sm">Edit</a>|
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
        </td>
        </tr>
    @empty
        <tr>
            <td colspan="5" class="text-center">
                <h4>Data Belum Ada</h4>
                <p>Silahkan tambah data terlebih dahulu!</p>
            </td>
        </tr>
    @endforelse
    </tfoot>
  </table>
@endsection