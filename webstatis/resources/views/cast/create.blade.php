@extends('layout.main')
@section('title')
    Create Cast
@endsection
@section('content')
<form method="post" action="/cast">
    @csrf
    <label for="nama">Nama : </label><br>
    <input type="text" id="nama" name="nama" ><br><br>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label for="umur">Umur :</label><br>
    <input type="number" id="umur" name="umur"><br><br>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <label for="bio">Bio</label><br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection