Hari 10 - Berlatih SQL

1. Membuat Database 
create database myshop;

2.Membuat tabel users, categories, items 
create table users(
    id int auto_increment primary key,
    name varchar(255),
    email varchar(255),
    password varchar(255)
);
create table categories(
    id int auto_increment primary key,
    name varchar(255),
);
create table items(
    id int auto_increment primary key,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id	int,
    CONSTRAINT category_id FOREIGN KEY (category_id) REFERENCES categories (id)
);

3. Memasukan data pada tabel
insert into users (name, email, password) 
values ('John Doe', 'john@doe.com', 'john123'),
(Jane Doe', 'jane@doe.com', 'jenita123');

insert into categories (name) 
values ('gadget'), ('cloth'), ('men'), ('women'), ('branded');

insert into items (name, description, price, stock, category_id)
values ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);

4. Mengambil data dati tabel 
a. Mengambil data users 
=> select id, name, email from users;
b. Mengambil data items
=> select * from items where price>1000000;
=> select * from items where name like '%watch%';
c. mengambil data items join dengan kategori
=> select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id=categories.id;

5. mengubah data items
update items set price=2500000 where name='Sumsang b50' and id=1;